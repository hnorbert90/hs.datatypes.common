﻿namespace HS.DataTypes.Common
{
    using System.Collections.Generic;

    public class ValueObject<TValue, TValueObject> where TValueObject : ValueObject<TValue, TValueObject>, new()
    {
        protected virtual void Validate()
        {
        }

        public virtual TValue Value { get; protected set; }

        public static TValueObject From(TValue value)
        {
            TValueObject valueObject = new TValueObject 
            { 
                Value = value
            };

            valueObject.Validate();

            return valueObject;
        }

        protected virtual bool Equals(ValueObject<TValue, TValueObject> other)
        {
            return EqualityComparer<TValue>.Default.Equals(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            if(obj is null)
            {
                return false;
            }

            if(ReferenceEquals(this, obj))
            {
                return true;
            }

            if(GetType() != obj.GetType())
            {
                return false;
            }

            if(obj.GetType() == GetType() && obj is TValueObject valueObject)
            {
                return valueObject.Value.Equals(Value);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return EqualityComparer<TValue>.Default.GetHashCode(Value);
        }

        public static bool operator ==(ValueObject<TValue, TValueObject> left, ValueObject<TValue, TValueObject> right)
        {
            if(left is null && right is null)
                return true;

            if(left is null || right is null)
                return false;

            return left.Equals(right);
        }

        public static bool operator !=(ValueObject<TValue, TValueObject> left, ValueObject<TValue, TValueObject> right)
        {
            return !(left == right);
        }

        public static implicit operator TValue(ValueObject<TValue, TValueObject> valueObject)
        {
            return valueObject.Value;
        }

        public static explicit operator ValueObject<TValue, TValueObject>(TValue value)
        {
            return From(value);
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}
